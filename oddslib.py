# Raj Adhikari 2016
from bs4 import BeautifulSoup
from urllib import parse
import os
import sys
import requests
import time
import re
import logging
import json

# chcp 65001 <- for windows to change code page
################################################################################
class HttpFailed(Exception):
    def __init__(self, arg):
        self.message = arg

class NoParams(Exception):
    def __init__(self, arg):
        self.message = arg
################################################################################

sport_code = {
    "soccer" : "E-1-2-0-0-0",
    "tennis" : "E-3-2-0-0-0"
}

btypes = {
    1 : 1, #1X2
    2 : 3  #12
}

#UNUSED
commissions = {
    "7": 96,
    "39": 95,
    "42": 95,
    "44": 95,
    "113": 95,
    "390": 99
}
def csv_soccer_line(odds, b, isList, now, oddsall):
    exchange = False
    if b in commissions:
        exchange = True
    backodds = ",,"
    layodds = ",,"
    b1v = ""
    bxv = ""
    b2v = ""
    l1v = ""
    lxv = ""
    l2v = ""
    try:
        if b in oddsall["back"][sport_code["soccer"]]["volume"]:
            vol = oddsall["back"][sport_code["soccer"]]["volume"][b]
            if isList:
                try:
                    b1v = vol[0]
                except:
                    pass
                try:
                    bxv = vol[1]
                except:
                    pass
                try:
                    b2v = vol[2]
                except:
                    pass
            else:
                try:
                    b1v = vol["0"]
                except:
                    pass
                try:
                    bxv = vol["1"]
                except:
                    pass
                try:
                    b2v = vol["2"]
                except:
                    pass
    except:
        pass

    try:
        if b in oddsall["lay"][sport_code["soccer"]]["volume"]:
            vol = oddsall["lay"][sport_code["soccer"]]["volume"][b]
            if isList:
                try:
                    l1v = vol[0]
                except:
                    pass
                try:
                    lxv = vol[1]
                except:
                    pass
                try:
                    l2v = vol[2]
                except:
                    pass
            else:
                try:
                    l1v = vol["0"]
                except:
                    pass
                try:
                    lxv = vol["1"]
                except:
                    pass
                try:
                    l2v = vol["2"]
                except:
                    pass
    except:
        pass


    if not exchange:
        if isList:
            backodds = "{},{},{}".format(odds[0], odds[1], odds[2])
        else:
            backodds = "{},{},{}".format(odds["0"], odds["1"], odds["2"])
    else:
        if isList:
            backodds = "{},{},{}".format(odds[0], odds[1], odds[2])

            lay = oddsall["lay"][sport_code["soccer"]]["odds"][b]

            l1 = ""
            try:
                l1 = lay[0]
            except:
                pass
            l2 = ""
            try:
                l2 = lay[1]
            except:
                pass
            l3 = ""
            try:
                l3 = lay[2]
            except:
                pass
            layodds = "{},{},{}".format(l1, l2, l3)

        else:
            backodds = "{},{},{}".format(odds["0"], odds["1"], odds["2"])

            lay = oddsall["lay"][sport_code["soccer"]]["odds"][b]

            l1 = ""
            try:
                l1 = lay["0"]
            except:
                pass
            l2 = ""
            try:
                l2 = lay["1"]
            except:
                pass
            l3 = ""
            try:
                l3 = lay["2"]
            except:
                pass
            layodds = "{},{},{}".format(l1, l2, l3)


    return "{},{},{},{},{},{},{},{},{},{}\n".format(bookmaker_name(b), backodds, now, b1v, bxv, b2v, layodds, l1v, lxv, l2v)


# Bookmaker, 1, 2, Back 1 Volume, Back 2 Volume, Lay 1, Lay 2, Lay 2 Volume, Lay 2 Volume
def csv_tennis_line(odds, b, isList, now, oddsall):
    exchange = False
    if b in commissions:
        exchange = True
    backodds = ",,"
    layodds = ",,"
    b1v = ""
    b2v = ""
    l1v = ""
    l2v = ""

    try:
        if b in oddsall["back"][sport_code["tennis"]]["volume"]:
            vol = oddsall["back"][sport_code["tennis"]]["volume"][b]
            if isList:
                try:
                    b1v = vol[0]
                except:
                    pass
                try:
                    b2v = vol[1]
                except:
                    pass
            else:
                try:
                    b1v = vol["0"]
                except:
                    pass
                try:
                    b2v = vol["1"]
                except:
                    pass
    except:
        pass

    try:
        if b in oddsall["lay"][sport_code["tennis"]]["volume"]:
            vol = oddsall["lay"][sport_code["tennis"]]["volume"][b]
            if isList:
                try:
                    l1v = vol[0]
                except:
                    pass
                try:
                    l2v = vol[1]
                except:
                    pass
            else:
                try:
                    l1v = vol["0"]
                except:
                    pass
                try:
                    l2v = vol["1"]
                except:
                    pass
    except:
        pass

    if not exchange:
        if isList:
            backodds = "{},{}".format(odds[0], odds[1])
        else:
            backodds = "{},{}".format(odds["0"], odds["1"])
    else:
        if isList:
            backodds = "{},{}".format(odds[0], odds[1])

            lay = oddsall["lay"][sport_code["tennis"]]["odds"][b]
            l1 = ""
            try:
                l1 = lay[0]
            except:
                pass
            l2 = ""
            try:
                l2 = lay[1]
            except:
                pass
            layodds = "{},{}".format(l1, l2)
        else:
            backodds = "{},{}".format(odds["0"], odds["1"])

            lay = oddsall["lay"][sport_code["tennis"]]["odds"][b]

            l1 = ""
            try:
                l1 = lay["0"]
            except:
                pass
            l2 = ""
            try:
                l2 = lay["1"]
            except:
                pass
            layodds = "{},{}".format(l1, l2)

    return "{},{},{},{},{},{},{},{}\n".format(bookmaker_name(b), backodds, now, b1v, b2v, layodds, l1v, l2v,)

csv_line = {
    "soccer" : csv_soccer_line,
    "tennis" : csv_tennis_line,
}

csv_headers = {
    "soccer" : "Bookmaker,Back 1,Back X,Back 2,Time,Back 1 Volume,Back X Volume,Back 2 Volume,Lay 1,Lay X,Lay 2,Lay 2 Volume,Lay X Volume,Lay 2 Volume\n",
    "tennis" : "Bookmaker,Back 1,Back 2,Time,Back 1 Volume,Back 2 Volume,Lay 1,Lay 2,Lay 2 Volume,Lay 2 Volume\n"
}
################################################################################
def status(r):
    logging.debug("HTTP status: {}".format(r.status_code))
    if r.status_code != 200:
        logging.warn("!!!")
        if r.status_code == 404: logging.critical("League doesn't exist! -  Check program arugments.")
        raise HttpFailed(str(r.status_code))

def fetch(src, host = "http://www.oddsportal.com/"):
    logging.debug("Fetching page...")
    logging.debug("Requesting {}/{}".format(host, src))
    retries = 3
    retry = 0
    wait_s = 5
    success = False
    while not success and retry < retries:
        try:
            r = requests.get("{}/{}".format(host, src))
            status(r)
            success = True
            return r.content
        except:
            retry += 1
            logging.warn("Unable to get {}. No connection ({} of {})".format(src, retry, retries))
            time.sleep(wait_s)

    if not success:
        raise requests.ConnectionError("No Connection")

# debug fucntion, there is probably something in the standard library that does this.
def dump(s, n = ""):
    with open("dump{}".format(n), "w") as f:
        f.write(str(s))
        f.close()

def discover(content, sport, country, league):
    """
    discover upcoming matches in a league
    """
    #parsing acted wierdly, tokenized instead
    tokstart = "href=\"/{}/{}/{}/".format(sport, country, league)
    tokend = "\">|/"
    upcoming = []
    matches = []
    match_times = []
    big_soup = BeautifulSoup(content, 'html.parser')
    # extract table from markup
    small_soup = BeautifulSoup(str(big_soup.find_all('tbody')), 'html.parser')
    # extract table cells with time information
    dates = big_soup.find_all('td', class_="datet")

    if len(dates) == 0 :
        logging.info("No upcoming matches")

    # very, VERY, ugly algorithim. but it works
    for date in dates:
        try:
            date_strip = str(date['class'][2]).split("t")[1].split("-")[0]
            match_times.append(date_strip)
        except:
            pass

    for links in small_soup.find_all('a')[2:]: #ignore heading links
        if len(str(links).split(tokstart)) == 2:
            # ignore junk dupes
            m = str(links).split(tokstart)[1]
            match = re.split('"|/',m)[0]
            if match != "" and match not in matches:
                matches.append(match)

    for match in range(len(matches)):
        match_sig = "{}@{}".format(matches[match], match_times[match])
        if match_sig not in upcoming:
            logging.debug("Found new match: {}".format(match_sig))
            upcoming.append(match_sig)

    return upcoming

def datecheck(upcoming, maxtime):
    logging.info("Checking match times...")
    logging.debug("Looking ahead {} seconds".format(maxtime))
    temp = []
    for match in upcoming:
        date = int(match.split("@")[-1])
        diff = date - int(time.time())
        if diff < maxtime:
            logging.info("Found {} ({})".format(match, diff ))
            temp.append(match)
        else:
            logging.debug("Discarding {}. Too far ahead.".format(match))
    return temp

def strip_params(match, SCL):
    """
    Extract the filename information for the javascript file with odds
    information from the match page.

    the target characters are the params passed to a PageEvent function
    """
    link = match.split("@")[0]
    path = "{}/{}/{}/{}".format(SCL["sport"], SCL["country"], SCL["league"], link)
    # fetch page markup
    content = fetch(path)
    markup = str(content)

    logging.debug("Scanning {} match page".format(link))
    if len(markup.split("PageEvent(")) != 2:
        logging.critical("Unable to find filename data")
        raise false

    params = markup.split("PageEvent(")[-1].split(");")[0]

    return params


def strip_odds(content, sport):
    """
    Extract the odds infromation from the js file
    """
    logging.debug("Extracting odds information...")
    markup = str(content)
    # since the object with odds information is actaully a param to a function
    # cast the markup as a string and search split with substrings
    odds = markup.split("dat',")[-1].split(");")[0] # for some reason "\'" is part of bytestream
    # prase the json into a dict
    dump(odds)
    data_all = json.loads(odds)
    if "oddsdata" in data_all["d"]:
        data_odds = data_all["d"]["oddsdata"]["back"][sport_code[sport]]["odds"]
    else:
        # Denied
        logging.warn("Unabled to get odds data at this time (Not allowed).")
        return -1

    return {"back" : data_odds, "all" : data_all["d"]["oddsdata"]}

# ###########################################################################
# url += '' + page.params.versionId;
# url += '-' + page.params.sportId;
# url += '-' + page.params.id;
# url += '-' + page.bettingType;
# url += '-' + page.scopeId;
# url += '-' + page.process(page.params.xhash);
# url += '.' + 'dat';
# if (Debug.isOpen()) {
#     $('#download_event_feed_save').attr('href', url);
#     $('#download_event_feed_save').removeClass('hidden');
#     if ($('#input_event_feed_save').val()) {
#         url = '/feed/prematch/' + $('#input_event_feed_save').val();
#         page.feedHash = 'hard Reload'
#     }
# }
############################################################################
# Javascript used on oddsportal to build url
# eg: http://fb.oddsportal.com/feed/match/1-1-K2bWNGpt-1-2-yja52.dat
############################################################################
def buildurl(jsparams, match):
    """
    Contruct the filename for the js file that contains odds information
    """
    logging.debug("Building data file url...")

    params = json.loads(jsparams)

    versionId = params['versionId']
    sportId = params['sportId']
    m_id = params['id']
    scopeId = 2
    xhash = params['xhash']
    # "decode"
    xhash = parse.unquote(xhash)
    bettingType = btypes[sportId]

    url = "{}-{}-{}-{}-{}-{}.dat".format(versionId,
                                         sportId,
                                         m_id,
                                         bettingType,
                                         scopeId,
                                         xhash)

    logging.debug(url)

    return url

def createdirs(pname):
    if not os.path.exists(pname):
        # handle race condition
        try:
            os.makedirs(pname)
        except OSError as error:
            if error.errno != errno.EEXIST:
                raise # something bad

def printcsv(oddsdata, match, args):
    """
    Export csv with odds data
    """
    teams = "-".join(match.split("-")[:-1])
    mtime = match.split("@")[-1]
    fname = "{}-{}-{}--{}.csv".format(args["sport"], args["country"], args["league"], teams)
    pname = "{}/".format(args["sport"])
    wait_s = 5
    retries = 5
    retry = 0
    saved =  False
    odds = oddsdata["back"]
    createdirs(pname)

    # for some reason, oddsport sometimes store odds data as an array of ints, and sometimes as an object
    check = list(odds.values())[0]
    isList = True if isinstance(check, list) else False
    now = int(time.time())

    logging.info("Generating {}".format(fname))
    while not saved and retry < retries:
        try:
            mode = 'a'
            if args["replace"] == True:
                mode = 'w'
            with open(pname+fname, mode) as f:
                logging.debug("Saving {}".format(fname))
                if os.stat(pname+fname).st_size == 0:
                    f.write("{}\n".format(mtime))
                    f.write(csv_headers[args["sport"]])
                for b in odds:
                    # bname = bookmaker_name(b)
                    line = csv_line[args["sport"]](odds[b], b, isList, now, oddsdata["all"])
                    f.write(line)
                f.close()
                saved = True
        except PermissionError:
            retry += 1
            logging.warn("Permission denied! Cannot save {}. Trying again in 5 seconds... ({} of {})".format(fname, retry, retries))
            time.sleep(wait_s)
    if not saved:
        logging.warn("Unable to save {}. Skipping untill next refresh.".format(fname))

def bookmaker_name(b):
    """
    Get human readable bookie name
    """
    with open('bookies.json','r') as f:
        bookies = json.loads(f.read())
        try:
            return bookies[b]["WebName"]
        except KeyError:
            return "Unknown ({})".format(b)
    return b

# Aglorithim taken from oddsportal
def commission_odd(odd, commission, isBack):
    val = 0
    commission = commission / 100

    if isBack:
        val = (odd - 1) * commission + 1
    else:
        val = (commission + odd - 1) / commission

    return round(val, 2)

def prepRequest(url, SCL, match):
    path = "{}/{}/{}/{}/".format(SCL["sport"], SCL["country"], SCL["league"], match.split("@")[0])
    preppedHeaders = {"Host": "fb.oddsportal.com",
                    "User-Agent" : "Mozilla/5.0 (X11; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0" ,
                    "Accept" : "*/*",
                    "Accept-Language" : "en-US,en;q=0.5",
                    "Accept-Encoding": "gzip, deflate",
                    "Referer": "http://www.oddsportal.com/",
                    "DNT": "1",
                    "Connection": "keep-alive"}
    preppedHeaders["Referer"] = "http://www.oddsportal.com/{}".format(path)

    req = requests.Request('GET', "http://fb.oddsportal.com/feed/match/{}".format(url), headers=preppedHeaders)
    prepped = req.prepare()
    return prepped
