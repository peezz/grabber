# Raj Adhikari 2016
from oddslib import *
from multiprocessing import Pool
import multiprocessing
import sys
import time
import logging
import os
import argparse
import traceback
import requests
# new comment
# chcp 65001 <- for windows to change code page
##########################
logging.basicConfig(filename='grab.log', filemode='w', format='%(asctime)s -- %(levelname)-9s%(funcName)-12s >> %(message)s', datefmt='%Y-%m-%d:%H:%M:%S' ,level=logging.DEBUG)
log = logging.getLogger()
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("oddslib").setLevel(logging.NOTSET)

# Log to stdout aswell
formatterStdout = logging.Formatter('[%(asctime)s][%(levelname)s] >> %(message)s', datefmt='%Y-%m-%d:%H:%M:%S')
consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setLevel(logging.INFO)
consoleHandler.setFormatter(formatterStdout)

log.addHandler(consoleHandler)

urlcache = {}
supported = ["soccer", "tennis"]

############
def now():
    return int(time.time())

def process(match, args):
    global urlcache
    dataurl = ""
    if match in urlcache:
        dataurl = urlcache[match]
    else:
        try:
            params = strip_params(match, args)
        except HttpFailed:
            log.warn("Failed to retrieve data for {}".format(match))
        except NoParams:
            log.warn("File information for {} not found.".format(match))
        except requests.ConnectionError:
            log.warn("Unable to open connection. Skipping {}...".format(match))
        else:
            dataurl = buildurl(params, match)
            urlcache[match] = dataurl

    if dataurl == "":
        log.warn("Unable to resolve match URL")
        return -1

    try:
        raw_data = ""
        with requests.Session() as s:
            prepped = prepRequest(dataurl, args, match)
            resp = s.send(prepped)
            if resp.status_code != 200: raise HttpFailed(str(resp.status_code))
            raw_data = resp.text
    except HttpFailed:
        log.warn("Cannot retrieve {} at this time".format(dataurl))
    except requests.ConnectionError:
        log.warn("Unable to open connection. Skipping {}...".format(match))
    except Exception as e:
        log.warn("Unable to retrive betting infromation ({})".format(match))
    else:
        oddsdata = strip_odds(raw_data, args["sport"])
        if oddsdata != -1: printcsv(oddsdata, match, args)

def main():

    upcoming = []
    running = True
    lastdiscovery = 0
    lastrefresh = 0

    # seconds to wait before refreshing upcoming games.
    DISCOVERTIME = 3600

    parser = argparse.ArgumentParser()
    parser.add_argument('sport', help='Sport name in lowercase.', choices=supported, metavar='sport')
    parser.add_argument('country', help='Country name in lowercase. Replace spaces with "-". eg "Costa Rica" -> "costa-rica"')
    parser.add_argument('league', help='League name in lowercase. Replace spaces with "-". eg "Premier League" -> "premier-league"')
    parser.add_argument('-s', '--seconds', type=int, default=60, help='Number of seconds before refreshing odds data.')
    parser.add_argument('-d', '--days', type=int, default=1, help='Number of days ahead to look for matches')
    parser.add_argument('-r', '--replace', action="store_true", default=False, help='Replace CSV, instead of appending.')

    args = vars(parser.parse_args())
    future = args['days'] * 86400 # seconds in X days
    UPDATEINTERVAL = args['seconds']

    praser = None

    leagueurl = "{}/{}/{}".format(args["sport"], args["country"], args["league"])

    #dump info
    log.info("Sport: {}".format(args["sport"]))
    log.info("Country: {}".format(args["country"]))
    log.info("League: {}".format(args["league"]))
    log.info("Match refresh interval: {}".format(args["seconds"]))
    log.info("Looking {} day(s) ahead".format(args["days"]))
    log.info("Starting program...")

    while running:
        try:
            if now() - lastdiscovery > DISCOVERTIME:
                try:
                    leaguepage = fetch(leagueurl)
                except HttpFailed:
                    log.warn("Unable to fetch league information. Exiting...")
                    sys.exit(-1)
                except requests.ConnectionError:
                    log.warn("Unable to fetch league information. Exiting...")
                    sys.exit(-1)
                else:
                    found = discover(leaguepage, args["sport"],  args["country"],  args["league"])
                    upcoming = datecheck(found, future)
                    if len(upcoming) == 0:
                        log.info("No upcoming matches with {} day(s) found! Updating in {} seconds".format(args["days"], DISCOVERTIME))
                    lastdiscovery = now()

            if now() - lastrefresh > UPDATEINTERVAL and len(upcoming) != 0:
                # process each match concurrently
                argarray = [args for match in upcoming]
                log.info("Updating matches. Please do not exit the program yet.")
                pool = Pool(os.cpu_count())
                pool.starmap(process, zip(upcoming, argarray))
                pool.close()
                pool.join()

                lastrefresh = now()
                log.info("Matches updated! refreshing in {} seconds".format(UPDATEINTERVAL))
                log.info("Press ctrl+c to exit")
        except KeyboardInterrupt:
            log.info("Interrupt signal found. Exiting program...")
            exit(1)
        except Exception as e:
            #please no
            log.exception("Encountered unexpected behavior: {}.\n\tUnable to handle, exiting...".format(e))
            exit(-1)


if __name__ == '__main__':
    main()
